const GeburtagsApp = {
    data() {
        return {
            // Alle Datensätze
            personen: [],/*
                { name: 'Anna', geburtsdatum: '1998-02-01', geschenkidee: 'AAA' },
                { name: 'Berta', geburtsdatum: '2007-05-27', geschenkidee: 'BBB' },
                { name: 'Carla', geburtsdatum: '2000-03-03', geschenkidee: 'CCC' },
                { name: 'Dieter', geburtsdatum: '2000-04-03', geschenkidee: 'DDD' }
            */

            // CREATE
            personNeu: {},

            // DELETE
            personZuLoeschen: {},

            // UPDATE
            personUpdate: {},

            // Zwischenspeicher
            personZwischenspeicher: {},

            // Sichtbarkeit der Views
            readVisible: true,
            createVisible: false,
            deleteVisible: false,
            updateVisible: false
        };
    },

    methods: {
        // R E A D
        buttonNeuClick() {
            // CREATE View anzeigen
            this.showCreateView();

            // Standardwerte für neuen Datensatz anzeigen
            this.personNeu.name = 'Neu';
            this.personNeu.geburtsdatum = '1966-06-06';
            this.personNeu.geschenkidee = 'Grüne Socken';
        },

        buttonLoeschenClick(person) {
            // zu löschende Person speichern
            this.personZuLoeschen = person;

            // DELETE View anzeigen
            this.showDeleteView();
        },

        buttonBearbeitenClick(person) {
            // Referenz auf person zwischenspeichern
            this.personZwischenspeicher = person;

            // UPDATE View anzeigen
            this.showUpdateView();

            // aktuelle Daten anzeigen
            this.personUpdate.name = person.name;
            this.personUpdate.geburtsdatum = person.geburtsdatum;
            this.personUpdate.geschenkidee = person.geschenkidee;
        },

        // C R E A T E
        buttonSpeichernClick() {
            // Neuen Datensatz erstellen
            const neuePerson = {
                name: this.personNeu.name,
                geburtsdatum: this.personNeu.geburtsdatum,
                geschenkidee: this.personNeu.geschenkidee
            };

            // Neuen Datensatz zur Liste hinzufügen
            this.personen.push(neuePerson);

            // Alle Daten in localStorage speichern
            this.speichernInLocalStorage();

            // READ View anzeigen
            this.showReadView();
        },

        // D E L E T E
        buttonJaClick() {
            // Element 'personZuLoeschen' löschen
            const index = this.personen.indexOf(this.personZuLoeschen);
            this.personen.splice(index, 1);

            // Alle Daten in localStorage speichern
            this.speichernInLocalStorage();            

            // READ View anzeigen
            this.showReadView();
        },

        // U P D A T E
        buttonUpdateSpeichernClick() {
            // geänderten Daten speichern
            this.personZwischenspeicher.name = this.personUpdate.name;
            this.personZwischenspeicher.geburtsdatum = this.personUpdate.geburtsdatum;
            this.personZwischenspeicher.geschenkidee = this.personUpdate.geschenkidee;

            // Alle Daten in localStorage speichern
            this.speichernInLocalStorage();

            // READ View anzeigen
            this.showReadView();
        },

        // Abbrechen für alle Views
        buttonAbbrechenClick() {
            // READ View anzeigen
            this.showReadView();
        },

        // show-Methoden
        showReadView() {
            this.readVisible = true;
            this.createVisible = false;
            this.deleteVisible = false;
            this.updateVisible = false;
        },

        showCreateView() {
            this.readVisible = false;
            this.createVisible = true;
            this.deleteVisible = false;
            this.updateVisible = false;
        },

        showDeleteView() {
            this.readVisible = false;
            this.createVisible = false;
            this.deleteVisible = true;
            this.updateVisible = false;
        },

        showUpdateView() {
            this.readVisible = false;
            this.createVisible = false;
            this.deleteVisible = false;
            this.updateVisible = true;
        },
        
        // Hilfsmethoden
        deutschesDatumsformat(rfcDatum){
            const date = new Date(rfcDatum);
            const options = {
                weekday: "long",
                day: "numeric",
                month: "long",
                year: "numeric"
            };
            const stringDate = date.toLocaleDateString('de-DE', options);            
            return stringDate;
        },

        // localStorage
        speichernInLocalStorage(){
            const jsonString = JSON.stringify(this.personen);
            localStorage.setItem('geburtstage', jsonString);
        },

        ladenVonLocalStorage(){
            const data = localStorage.getItem('geburtstage');
            if(data){
                // Data in localStorage vorhanden
                this.personen = JSON.parse(data);
            }else{
                // // keine Data in localStorage vorhanden
                this.personen = [];
            }
        }
    },

    mounted(){
        // Wird bei Programmstart einmal aufgerufen
        // Daten von localStorage lesen
        this.ladenVonLocalStorage();
    }
};
Vue.createApp(GeburtagsApp).mount('#app');

// deleted pages branch